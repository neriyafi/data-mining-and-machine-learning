# Neriya Friss
#Task 1

############# Question1

up.norm <- function(mean,sd) {
  x <- rnorm(1,mean,sd)
  return(ceiling(x))
}

up.norm(19,3)

############# Question2

project.length <-function(x) {
  lengthABCF <- (9 + up.norm(20,sqrt(18)))
  lengthADEF <- (14 + up.norm(15,sqrt(13)))
  if(lengthABCF>lengthADEF){
    critical <- "Critical path: A-B-C-F"
    v <- c(critical,lengthABCF)
  }
  else {
    critical <- "Critical path: A-D-E-F"
    v <- c(critical,lengthADEF)
  }
  return(v)
}

project.length()

############# Question3

samples <- 1:10000
project.time.vec <- sapply(samples,project.length)
project.timeint.Yalla.Beitar <- as.integer(project.time.vec)
project.timeint.Yalla.Beitar <- project.timeint.Yalla.Beitar[!is.na(project.timeint.Yalla.Beitar)]
colors = c ("black", "yellow")
hist(project.timeint.Yalla.Beitar,20, border = "black", col = colors )

############# Question4A

#probability <- function(x){
#i <- 10000
#counterABCF <- 0
#counterADEF <- 0
#while (i > 0) {
# x <- project.length()
 
#  if(x[1] == "Critical path: A-B-C-F"){
#    counterABCF <- counterABCF + 1
#  }
# else {
#   counterADEF <- counterADEF + 1 
# }
# i <- i-1
#}
#  probability.ABCF <- (counterABCF/(counterABCF + counterADEF))
#  return(probability.ABCF)
#}

#probability()

##4
samples <- 1:10000
y <- sapply(samples,project.length)
project.length.vec <- as.factor(y[1,])
project.length.vec1 <- summary.factor(project.length.vec)
project.length.vec1 <- as.integer(project.length.vec1)
probability <- project.length.vec1[1]/10000
probability
############# Question5
#���� ����� �� ���� ����� ��� ���� ��� ������� 35 �����
#��� ����� ������� ����� ����� ��� �������� ����� ���� 35 ���� 10 ���
probability35 <- function(x){
  i <- 10000
  counter.35 <- 0
  counter.not35 <- 0
  while (i > 0) {
    x <- project.length()
    if(x[2] == 35){
      counter.35 <- counter.35 + 1
    }
    else {
      counter.not35 <- counter.not35 + 1 
    }
    i <- i-1
  }
  probability.35 <- (counter.35/(counter.35 + counter.not35))
  return(probability.35)
}

Should.invest <- function(cost){
fine.that.could.save <- probability35() * 10000
if (fine.that.could.save < cost ) {
  return("no")
}
else{
  return("yes")
}
}
Should.invest(2000)
Should.invest(700)
Should.invest(699)

